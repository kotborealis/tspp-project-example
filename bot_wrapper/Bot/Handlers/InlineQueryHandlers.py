from Bot.BotMain import dispatcher
from aiogram import types
from aiogram.types import InputTextMessageContent, InlineQueryResultArticle
import hashlib

from Bot.BotMain import calc_proxy


@dispatcher.inline_handler(lambda q: q.query)
async def calc_inline(inline_query: types.InlineQuery):
    code, res = calc_proxy.get_expression_result(inline_query.query)
    if code != 0:
        return
    items = [
        InlineQueryResultArticle(
            id=hashlib.md5((inline_query.query.strip()).encode()).hexdigest(),
            title=inline_query.query.strip(),
            description=f'{inline_query.query.strip()} = {res}',
            input_message_content=InputTextMessageContent(message_text=f'{inline_query.query.strip()} = {res}',
                                                          parse_mode=None)
        )
    ]
    await inline_query.answer(results=items,
                              cache_time=10,
                              is_personal=False)

