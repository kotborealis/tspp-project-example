import typing
from typing import Tuple, Optional
import subprocess
import os


class ShuntingYardProxy:
    def __init__(self, calling_name: str):
        if not os.path.exists(calling_name):
            raise FileNotFoundError("Solving-core not found")
        self._calling = calling_name

    '''
    Функция получения результата от модуля подсчета выражений
    :argument expression — выражение, значение которого надо найти
    :return (code, result) — кортеж с кодом ошибки и результатом
    Коды ошибок:
    > 0 - все хорошо, результат выражения в result
    > 1 - ошибка в выражении, результат None
    > 2 - критичная ошибка доступа к модулю подсчета, результат None 
    '''
    def get_expression_result(self, expression: str) -> Tuple[int, Optional[str]]:
        try:
            process = subprocess.Popen([self._calling, expression],
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.STDOUT)
        except (FileNotFoundError, FileExistsError):
            return 2, None

        data = process.communicate("")
        exit_code = process.wait()
        if exit_code != 0:
            return 1, None
        return 0, data[0].decode('utf-8')


if __name__ == "__main__":
    calling = 'Stack.exe'
    proxy = ShuntingYardProxy(calling)
    code, res = proxy.get_expression_result("2+2*2")
    print(float(res))

