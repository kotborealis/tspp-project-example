#! /usr/bin/env bash

python3 -m pip install -r requirements.txt;
kill $(cat ./bot_pid) || true
nohup python3 ./BotLauncher.py >log.txt 2>&1 & 
echo $! > ./bot_pid