#include <iostream>
#include <string>
#include <stdexcept>

#include "ExpressionSolver.h"

enum Error {
	Ok = 0,
	InvalidExpression = 2,
	NoExpression = 4
};


int main(int argc, char** argv) {
	if (argc == 1) {
		std::cout << "��������� �� ��������";
		return NoExpression;
	}
	try {
		std::cout << ExpressionSolver::solve(std::string(argv[1]));
		return Ok;
	}
	catch (std::exception& e) {
		std::cout << e.what(); 
		return InvalidExpression;
	}
}