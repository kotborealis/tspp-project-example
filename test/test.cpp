#define CATCH_CONFIG_MAIN 
#include "ExpressionSolver.h"
#include "catch.hpp"

TEST_CASE("Simple operators") {
	SECTION("SUM") {
		REQUIRE(
			ExpressionSolver::solve("2+2") == 4
		);
		REQUIRE(
			ExpressionSolver::solve("2+2+2") == 6
		);
	}

	SECTION("SUB") {
		REQUIRE(
			ExpressionSolver::solve("2-2") == 0
		);
		REQUIRE(
			ExpressionSolver::solve("2-2-2") == -2
		);
	}
}

TEST_CASE("Order") {
	SECTION("MUL") {
		REQUIRE(
			ExpressionSolver::solve("(2+2)*2") == 8
		);
		REQUIRE(
			ExpressionSolver::solve("2+2*2") == 6
		);
	}
	SECTION("DIV") {
		REQUIRE(
			ExpressionSolver::solve("(2+2)/2") == 2
		);
	}
}

